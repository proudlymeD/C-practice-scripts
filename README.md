# My-practice-scripts

As the name suggests, these are scripts I made either for the purpose of learning a particular task, or for fun. Feel free to use/take anything you like, make sure to do your own due diligence when using these.

### Individual scripts

**calendar.c**: Simple command-line calendar that prints this month's dates, either using the current date or an arbitrary entered date. Mostly working. (6/22/2022)
**example_caseshift_func.sh**: A bash input flag function using case/esac and shift. (migrated on 11/07/2022)